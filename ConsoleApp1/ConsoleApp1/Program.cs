﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            MyStack<string> s1 = new MyStack<string>();

            Console.WriteLine(s1.size());
            Console.WriteLine(s1.top());

            s1.push("first");
            s1.push("second");

            Console.WriteLine(s1.empty());
            Console.WriteLine(s1.size());

            s1.push("third");
            s1.push("fourth");

            while (!s1.empty())
            {
                System.Console.WriteLine(s1.top());
                s1.pop();
            }

            s1.pop();
            Console.WriteLine(s1.empty());

            Console.ReadLine();
        }
    }
}
